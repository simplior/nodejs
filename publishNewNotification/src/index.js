const axios = require('axios');
const gql = require('graphql-tag');
const graphql = require('graphql');
const { print } = graphql;

let API_URL = "https://ggz57wt72jerpm2y3lhslwvrjq.appsync-api.ap-southeast-1.amazonaws.com/graphql"; // graphql http url
let API_KEY = "da2-m2jatutarfgsnhaou34ugr2k74	"; // production-key 

const publishNotificationStr = gql`
  mutation PublishNotification($message: String, $assignedToUserID: String) {
        publishNotification(message: $message, assignedToUserID: $assignedToUserID)
  }
`

exports.handler = async (event, context, callback) => {
    console.log("Debug from publish new note");
    console.log("Debug " + JSON.stringify(event));
    
    event.Records.forEach(async (record) => {
      console.log('Stream record: ', JSON.stringify(record, null, 2));

      if (record.eventName == 'INSERT' && record.dynamodb.NewImage) {
          var what = record.dynamodb.NewImage.note.S;
          var who = record.dynamodb.NewImage.assignedToUserID.S;
          console.log('what '+ what);
          console.log('who '+ who);
          var input = {
             assignedToUserID: who,
             message: what 
          };
          console.log(input);
          const graphqlData = await axios({
            url: API_URL,
            method: 'post',
            headers: {
              'x-api-key': API_KEY
            },
            data: {
              query: print(publishNotificationStr),
              variables: input
            }
          });
          console.log(graphqlData.data.data);
          console.log(graphqlData.data.errors);
      }
    });
    callback(null, `{ "message": "Successfully processed ${event.Records.length} records."}`);
};
